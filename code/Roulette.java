import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        RouletteWheel rouletteWheel = new RouletteWheel();
        int totalMoney = 1000;
        boolean keepGoing = true;
        while (keepGoing) {
            System.out.println("Do you wish to bet?  n to quit, any other key to continue");
            String answer = scanner.next();
            if(answer.equals("n")){
                break;
            }
            System.out.println("Enter the number you'd like to bet on.");
            int betNumber = scanner.nextInt();
            int betMoney = 1;
            System.out.println("How much would you like to bet?");
            betMoney = scanner.nextInt();
            while(betMoney <= 1 || totalMoney < betMoney){
                if (betMoney < 1){
                    System.out.println("You have to bet at least $1.");
                }
                if (totalMoney <= betMoney){
                    System.out.println("You don't have that much money.");
                }
                System.out.println("How much would you like to bet?");
                betMoney = scanner.nextInt();
            }
            
        
            rouletteWheel.spin();
            System.out.println("The correct number is " + rouletteWheel.getValue());
            if (rouletteWheel.getValue() == betNumber) {
                totalMoney += betMoney * 35;
                System.out.println("You won!");
            }else{
                totalMoney -= betMoney;
                System.out.println("You lost.");
            }
            if (totalMoney <= 0 ){
                keepGoing = false;
                System.out.println("You have no money left.");
            }
            System.out.println("You now have $" + totalMoney);
        }
        
    }
}